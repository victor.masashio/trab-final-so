Grupo: Gabriel Costa, Victor Masashi, Vin�cius Canuto

LINUX

1. Instalar GCC & Git.

	# Instalar GCC
	apt-get install gcc
	# Instalar git 
	apt-get install git	

2. Resgatar reposit�rio 

	# Navegar at� o diret�rio que deseja salvar o repositorio
	cd /home/nome_usuario/local_que_deseja_salvar_repositorio
	# reposit�rio https://gitlab.com/victor.masashio/trab-final-SO.git
	git clone https://gitlab.com/victor.masashio/trab-final-SO.git
	# O reposit�rio vai ser gerado

3. Compilacao do arquivo C.

	3.1. Navegar ate o repositorio
	# Navegar ate o diretorio
	cd /home/nome_usuario/local_que_deseja_salvar_repositorio/trab-final-SO.git
	# Compilar o arquivo 
	gcc nome_arquivo.c -o nome_arquivo.bin

4. Execucao do arquivo C

	# Executar arquivo
	./nome_arquivo.bin
	# O arquivo txt vai ser gerado ap�s a execu��o

5. Resultado

	# Os resultados serao gerados em um arquivo chamado processos.txt
	# Neste arquivo, serao salvos o PID dos dois processos que serao gerados
	# Para verificar o resultado do arquivo, pode-se usar os comandos:
	cat processos.txt;
	gedit processos.txt;
	nano processos.txt;
	vim processos.txt;


MAC 
1. Instalar GCC & Git
	# Voc� vai precisar do gerenciador de aplicativos homebrew do mac, para instalar o GCC e o GIT.
	#Segue o link abaixo, do passo a passo para a instala��o do brew.

	# https://brew.sh/index_pt-br.

	# Instala��o do gcc
	# $ brew search gcc
	# $ brew install gcc

	# Instala��o do git
	# $ brew search git
	# $ brew install git

2. Resgatar Reposit�rio
	# Navegar at� o diret�rio que deseja salvar o reposit�rio
	cd /caminho/para/diretorio/local_que_deseja_salvar_repositorio
	# reposit�rio https://gitlab.com/victor.masashio/trab-final-SO.git
	git clone https://gitlab.com/victor.masashio/trab-final-SO.git
	# O reposit�rio vai ser gerado


	
3. Compila��o do arquivo C

	3.1. Navegar at� o reposit�rio
	# Navegar at� o diret�rio
	cd /caminho/para/diretorio/local_que_deseja_salvar_repositorio/trab-final-SO.git
	# Compilar o arquivo 
	gcc nome_arquivo.c -o nome_arquivo.bin
	# O arquivo nome_arquivo.bin que � o execut�vel do arquivo C ser� gerado.

4. Execu��o do arquivo C 
	# Execu��o do arquivo bin�rio
	./nome_arquivo.bin
	# O arquivo txt vai ser gerado ap�s a execu��o

5. Resultado

	# Os resultados ser�o gerados em um arquivo chamado processos.txt
	# Neste arquivo, ser�o salvos o PID dos dois processos que ser�o gerados
	# Para verificar o resultado do arquivo, pode-se usar os comandos:
	cat processos.txt;
	gedit processos.txt;
	nano processos.txt;
	vim processos.txt;









