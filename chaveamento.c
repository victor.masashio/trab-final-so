#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

/* Livro de SO: PDF(pag.91)   LIVRO(pag.73)*/

/* Processos irão escrever buffers em um arquivo txt que os processos criarão! identificando quais processos o executou! */
/* EX: no arquivo, estar escrito: PROCESSO A \n PROCESSO B \n .... e assim por diante*/
/* > Os dois processos irão utilizar o mesmo arquivo txt para escrever! < */
pid_t regiao_critica(pid_t processo, int identificador);
void regiao_nao_critica(pid_t processo);


int turn=0; /* Variavel global utilizada para controlar a entrada de processos na regiao critica */
FILE *arquivo;


pid_t regiao_critica(pid_t processo, int identificador){	
	if (identificador == 1)
	{
		printf("Valor de turn dentro da regiao critica: %d\n\n", turn);
		printf("Processo [A] dentro da Regiao Critica: %d\n", processo);
		arquivo = fopen("processo.txt", "a+");
	if (arquivo == NULL)
	{
		printf("Erro ao criar arquivo!\n");
		exit(1);
	}
		fprintf(arquivo, "Processo A: %d\n", processo);
		fclose(arquivo);
	}

	else if (identificador == 2)
	{
		printf("Valor de turn dentro da regiao critica: %d\n\n", turn);
		printf("Processo [B] dentro da Regiao Critica: %d\n", processo);	
		arquivo = fopen("processo.txt", "a+");
	if (arquivo == NULL)
	{
		printf("Erro ao criar arquivo!\n");
		exit(1);
	}
		fprintf(arquivo, "Processo B: %d\n", processo);
		fclose(arquivo);
	}

	return processo;
	

}

void regiao_nao_critica(pid_t processo)
{
	while(1)
	{
		if (turn != 0)
		{
			pid_t processoA = processo;
			//Processo A entrando na regiao critica
			printf("::::::A::::::\n");
			printf("processo A(%d) entrando na regiao nao critica\n",processoA);
			printf("Valor de turn(A): %d\n", turn);
			printf("::::::A::::::\n");
			break;

		}

		else if (turn != 1)
		{
			pid_t processoB = processo;
			//processo B entrando na regiao critica
			printf("::::::B::::::\n");
			printf("processo B(%d) entrando na regiao nao critica\n",processoB);
			printf("Valor de turn(B): %d\n", turn);
			printf("::::::B::::::\n");
			break;
		}
	}
}

int main()
{
	
	int ct = 0;
	
	//Limitar a execução do processo
	while(ct <= 10)
	{

		while(turn != 1)
		{
			printf("--------------TRATANDO PROCESSO [A]---------------------\n");
			printf("A: turn antes de chavear: %d\n", turn);
			pid_t A;
			int identificador = 1; // Processo A
			A = getpid();
			A = regiao_critica(A, identificador);
			turn = 1;
			regiao_nao_critica(A);
			ct++;
			printf("A: turn depois de chavear: %d\n", turn);
			printf("--------------FIM DO TRATAMENTO DO PROCESSO [A]---------------------\n\n");
		}

		while(turn != 0)
		{
			printf("--------------TRATANDO PROCESSO [B]---------------------\n");
			printf("B: turn antes de chavear: %d\n", turn);
			pid_t B;
			int identificador = 2; // Processo B
			B = getppid();
			B = regiao_critica(B, identificador);
			turn = 0;
			regiao_nao_critica(B);
			ct++;
			printf("B: turn depois de chavear: %d\n", turn);
			printf("--------------FIM DO TRATAMENTO DO PROCESSO [B]---------------------\n\n");
		}
	}

	return 0;
}
